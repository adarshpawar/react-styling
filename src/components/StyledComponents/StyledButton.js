import styled from 'styled-components';

const StyledButton = styled.button`
    font: inherit;
    cursor: pointer;
    border: 1px solid #DAF7A6;
    background: #DAF7A6;
    color: #3D1108;
    padding: 0.5rem 2rem;
    
    :focus {
        outline: none;
    }

    :hover, :active {
        background: lightblue;
        color: blue;
        border-color: lightblue;
    }
`;

export default StyledButton;
