import styled from 'styled-components';

 const StyledParagraph = styled.p`
    color: #4F544B;
 `;

 export default StyledParagraph;