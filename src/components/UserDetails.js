import React from 'react';
import StyledParagraph from './StyledComponents/StyledParagraph';

const UserDetails = props => {
  return (
    <div>
      <StyledParagraph>
        <strong>Name</strong>: {props.name}, <strong>Age</strong>: {props.age}
      </StyledParagraph>
      <StyledParagraph>My name is {props.name} - and these are my details.</StyledParagraph>
    </div>
  );
};

export default UserDetails;