import React from 'react';
import StyledButton from '../components/StyledComponents/StyledButton';
const Button = props => {
  return (
    <StyledButton onClick={props.onClick} 
    style={{
        
    }}>
      {props.children}
    </StyledButton>
  );
};

export default Button;