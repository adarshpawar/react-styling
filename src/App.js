import React from 'react';
import UserSummary from './components/UserSummary';
import styled from 'styled-components';

const StyledDiv = styled.div`
  width: 90%;
  max-width: 40rem;
  margin: 2rem auto;
  border: 1px solid #ccc;
  padding: 1rem;
`;

function App() {
  return (
    <StyledDiv>
      <UserSummary />
    </StyledDiv>
  )
}
export default App;